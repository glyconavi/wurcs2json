package org.glycoinfo.WURCSFramework.json;

public class Inputdata {
	
	
	String id;
	String wurcs;
	
	
	public void setInputdata (String a_id, String a_wurcs){
		this.id = a_id;
		this.wurcs = a_wurcs;
	} 
	
	
	public String getId() {
		return this.id;
	}
	
	public String getWURCS() {
		return this.wurcs;
	}
}
