package org.glycoinfo.WURCSFramework.json.exec;

import java.util.LinkedList;

import org.glycoinfo.WURCSFramework.json.Inputdata;

import org.glycoinfo.WURCSFramework.json.JsonWURCSGraph;

//import org.glycoinfo.WURCSFramework.json.WURCSJsonConverter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.glycoinfo.WURCSFramework.util.WURCSFactory;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSGraph;
import org.glycoinfo.WURCSFramework.wurcs.array.WURCSArray;
import org.glycoinfo.WURCSFramework.wurcs.sequence2.WURCSSequence2;
import org.glycoinfo.WURCSFramework.json.JsonUtil;

public class JSONtest {
	
		public static void main(String[] args) throws Exception {
			
			// WURCS= ....
			//   or
			// id  WURCS=... id WURCS=... id WURCS=... id WURCS=... id WURCS=...
			LinkedList<Inputdata> o_input = new LinkedList<Inputdata>();
			String str_WURCS = "";
					
			if (args.length == 1 ) {
				try {
						Inputdata o_inp = new Inputdata();
						str_WURCS = args[0];
						//o_inp.setInputdata("", str_WURCS);
						//o_input.add(o_inp);
				} catch (Exception e) {
					System.err.println("error");
					e.printStackTrace();
				}				
			}
			else if (args.length > 1 ) {
				try {
					for(int i=0; i< args.length; i++) {				
						if (i % 2 != 0){
							Inputdata o_inp = new Inputdata();
							String strucID = args[0];
							str_WURCS = args[1];
							//o_inp.setInputdata(strucID, str_WURCS);
							//o_input.add(o_inp);
						}
					}
				} catch (Exception e) {
					System.err.println("error");
					e.printStackTrace();
				}
				
			}
			else {
				// G00055MO <-- GlyTouCan Accession Number
				String strucID = "test data: G00055MO";
				str_WURCS = "WURCS=2.0/4,4,3/[u2122h][a2112h-1b_1-5][a2112h-1a_1-5][a2112h-1b_1-5_2*NCC/3=O]/1-2-3-4/a4-b1_b3-c1_c3-d1";
				str_WURCS = "WURCS=2.0/5,10,9/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5][a1221m-1a_1-5]/1-1-2-3-1-4-3-1-4-5/a4-b1_a6-j1_b4-c1_c3-d1_c6-g1_d2-e1_e4-f1_g2-h1_h4-i1";
				str_WURCS = "WURCS=2.0/2,2,1/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1a_1-5]/1-2/a4-b1";
				//Inputdata o_inp = new Inputdata();
				//o_inp.setInputdata(strucID, str_WURCS);
				//o_input.add(o_inp);				
			}			
			
			//if (o_input.size() > 0 ) {
				try {
					
					String str_json = "";
					//for (int i = 0; i < o_input.size(); i++) {
						
						//WURCSFactory t_oFactory = new WURCSFactory(o_input.get(i).getWURCS());
					WURCSFactory t_oFactory = new WURCSFactory(str_WURCS);
					//System.out.println(str_WURCS);
						//WURCSGraph t_oGraph = t_oFactory.getGraph();
						
						
						
						WURCSArray ar_wurcs = t_oFactory.getArray();
						
						
//						System.out.println(t_oGraph.getBackbones().size());
//						System.out.println(t_oGraph.getModifications().size());
						
						//WURCSSequence2 t_oSeq = t_oFactory.getSequence();
						
											
						
				        // JSON変換用のクラス
				        //ObjectMapper mapper = new ObjectMapper();

				        try {
				            //JSON文字列に変換
				        	//str_json = mapper.writeValueAsString(t_oGraph);
				        	//str_json = JsonUtil.convert(t_oGraph);
				        	//str_json = JsonUtil.convert(t_oSeq);
				        	str_json = JsonUtil.convert(ar_wurcs);
				            System.out.println(str_json);
				        } catch (Exception e) {
				            e.printStackTrace();
				        }
					//}					
					//System.out.println(str_json);
				} catch (Exception e) {
					System.err.println("error");
					e.printStackTrace();
				}
			//}
			//else {
					//System.err.println("error");
			//}
		}
}
