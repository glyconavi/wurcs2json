package org.glycoinfo.WURCSFramework.json;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;

import org.glycoinfo.WURCSFramework.util.WURCSFactory;
import org.glycoinfo.WURCSFramework.wurcs.array.WURCSArray;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSGraph;


public class JsonUtil {
     /**
     * JSON文字列をDTOクラスへ変換する
     * @param dto
     * @param json
     * @return DTOobject
     * @throws IOException
     */
     public static <T> T parse(Class<T> dto, String json){
          ObjectMapper mapper = new ObjectMapper();
          try{
               return (T) mapper.readValue(json, dto);
          }catch(IOException e){
               return null;
          }
     }
     /**
     * DTOクラスのインスタンスをJSON文字列に変換する
     * @param dto
     * @return String
     * @throws JsonProcessingException
     */
     public static String convert(WURCSArray a_obj){
         ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
    	 //ObjectMapper mapper = new ObjectMapper();
    	 //ObjectMapper mapper = new ObjectMapper().setPropertyNamingStrategy(PropertyNamingStrategy.LOWER_CAMEL_CASE);
          
          try{
        	  
				//System.out.println(a_obj.getRESCount());
				//System.out.println(a_obj.getLINCount());

        	  
               String json = mapper.writeValueAsString(a_obj);
               //String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(dto);
               return json;
          }catch(JsonProcessingException e){
        	  System.out.println(e.toString());
               return "convet is null";
          }
     }
}