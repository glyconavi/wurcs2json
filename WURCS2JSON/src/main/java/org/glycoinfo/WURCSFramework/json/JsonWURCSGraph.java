package org.glycoinfo.WURCSFramework.json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.glycoinfo.WURCSFramework.util.WURCSException;
import org.glycoinfo.WURCSFramework.util.WURCSFactory;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSGraph;


/**
 * Class for JsonWURCSGraph
 * @author Issaku
 *
 */
public class JsonWURCSGraph {
	
	public WURCSGraph JSONgGraph;
	
	@JsonTypeInfo(use = Id.CLASS)
    static interface JSONgGraph {

	}
	
	

}
